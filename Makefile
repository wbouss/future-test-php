CFLAGS=-Wall -g

parser:
	php bin/console parse ./files/ToParse/

test:
	./vendor/bin/phpunit

cs:
	php vendor/friendsofphp/php-cs-fixer/php-cs-fixer fix
