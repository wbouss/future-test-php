## Environment 

For the moment only Working on linux

## Parser app

This app parse a directory, get the list of files and generate a new csv file in the right format

Result Directory: ./files/Result

## Use MakeFile

Command available: 
- parser
- test
- cs

## Use MakeFile

Command available: 
- parser
- test
- cs

## Run the Parser command

Run php bin/console parse ./files/ToParse/ 

## Execute tests

./vendor/bin/phpunit

## CodeSniffer Fixer 

php vendor/friendsofphp/php-cs-fixer/php-cs-fixer fix yourDirectory/
