<?php

namespace App\Tests;

use App\Enums\DownloadFreeTag;
use App\Enums\SubscriptionTag;
use App\Service\TagTransformer;
use PHPUnit\Framework\TestCase;

class TagTest extends TestCase
{
    public const SUBSCRIPTION_TAG = 'never_subscribed|subscription_unknown';
    public const SUBSCRIPTION_DOWNLOAD_TAG = 'has_downloaded_free_product|never_subscribed|expired_subscriber';

    /**
     * Test subscription tags.
     *
     * @test
     */
    public function subscription(): void
    {
        $tags = TagTransformer::getTagsFrom(self::SUBSCRIPTION_TAG);

        $this->assertTrue(2 === count($tags));

        foreach ($tags as $tag) {
            $this->assertTrue(SubscriptionTag::class === get_class($tag));
        }
    }

    /**
     * Test two diffent tag: SUBSCRIPTION and DOWNLOAD FREE.
     *
     * @test
     */
    public function multipleGroup(): void
    {
        $tags = TagTransformer::getTagsFrom(self::SUBSCRIPTION_DOWNLOAD_TAG);

        $this->assertTrue(3 === count($tags));

        foreach ($tags as $tag) {
            $this->assertTrue(
                SubscriptionTag::class === get_class($tag)
                || DownloadFreeTag::class === get_class($tag)
            );
        }
    }
}
