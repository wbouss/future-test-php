<?php

namespace App\Tests;

use App\Service\AppCodeTransformerService;
use PHPUnit\Framework\TestCase;

class AppCodeMappingTest extends TestCase
{
    public const NB_APP = 204;

    /**
     * Check if the initiazation is right.
     *
     * @test
     */
    public function appCodeInitialized(): void
    {
        $appCodeTransformerService = AppCodeTransformerService::getInstance();
        /* @var AppCodeTransformerService $appCodeTransformerService */

        $this->assertTrue(self::NB_APP === count($appCodeTransformerService->appCodeMapping));
    }

    /**
     * Check if AppCodeMApping file exist.
     *
     * @test
     */
    public function appCodeFileExist(): void
    {
        $appCodeTransformerService = AppCodeTransformerService::getInstance();

        $this->assertTrue(is_file($appCodeTransformerService::MAPPING_FILE));
    }
}
