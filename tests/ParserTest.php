<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class ParserTest extends KernelTestCase
{
    /**
     * One the parser command have been executed, we check existing a repository "Result" with his result file.
     *
     * @test
     */
    public function repositoryResultExist(): void
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);

        $command = $application->find('parse');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'files/ToParse/',
        ]);

        $output = $commandTester->getDisplay();
        $this->assertDirectoryExists('./files/Result', $output);
        $this->assertFileExists('./files/Result/result.csv', $output);
    }
}
