<?php

namespace App\Entity;

use App\Enums\Tag;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass=MagazineRepository::class)
 */
class Magazine
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $appCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $deviceId;

    /**
     * @ORM\Column(type="boolean")
     */
    private $contactable;

    /**
     * @ORM\ManyToMany(targetEntity=Tag::class, inversedBy="Magazine")
     */
    private $tags;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getAppCode(): ?string
    {
        return $this->appCode;
    }

    public function setAppCode(string $appCode): self
    {
        $this->appCode = $appCode;

        return $this;
    }

    public function getDeviceId(): ?string
    {
        return $this->deviceId;
    }

    public function setDeviceId(?string $deviceId): self
    {
        $this->deviceId = $deviceId;

        return $this;
    }

    public function getContactable(): ?bool
    {
        return $this->contactable;
    }

    public function setContactable(bool $contactable): self
    {
        $this->contactable = $contactable;

        return $this;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        $this->tags->removeElement($tag);

        return $this;
    }
}
