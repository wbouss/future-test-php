<?php

namespace App\Command;

use App\Service\ParserService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ParseCommand extends Command
{
    protected static $defaultName = 'parse';
    protected static $defaultDescription = 'Parse each file under a directory';

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    /** handle the command line.
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $directory = $input->getArgument('arg1');

        if (null === $directory || false === file_exists($directory)) {
            return 0;
        }

        $io->success(sprintf('Directory: %s', $directory));

        $count = ParserService::parse($directory);

        $io->success(sprintf('%s elements parsed', $count));

        return 0;
    }
}
