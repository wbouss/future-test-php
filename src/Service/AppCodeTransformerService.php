<?php

namespace App\Service;

class AppCodeTransformerService
{
    public const MAPPING_FILE = './files/Mapping/appCodes.ini';

    /** @var array */
    public $appCodeMapping;

    /**
     * @var Singleton
     */
    private static $instance;

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function __construct()
    {
        try {
            $file = fopen(self::MAPPING_FILE, 'r');

            // Skip the first line
            $line = fgets($file);
            while ($line = fgets($file)) {
                $l = explode('=', $line);

                if (count($l) >= 2) {
                    $l1 = str_replace('"', '', (trim($l[1])));
                    $l2 = str_replace('"', '', trim($l[0]));
                    $mapping[$l1] = $l2;
                }
            }
            fclose($file);

            $this->appCodeMapping = $mapping;
        } catch (Exception $exception) {
            $this->appCodeMapping = [];
        }
    }

    /**
     * Get the mapping app Code.
     */
    public static function getApp(string $appCode): string
    {
        return self::getInstance()->appCodeMapping[$appCode] ?? '';
    }
}
