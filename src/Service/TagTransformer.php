<?php

namespace App\Service;

use App\Enums\Tag;

class TagTransformer
{
    /**
     * Get Tags from string
     * Example of string : downloaded_free_single_issue_while_no_sub|never_subscribed\n.
     *
     * @return Tag[]
     */
    public static function getTagsFrom(string $value, string $separator = '|'): array
    {
        $tags = [];

        if (($values = explode($separator, $value)) === false) {
            return $tags;
        }

        foreach ($values as $value) {
            $tags[] = TagTransformer::getTagFrom(self::cleanValue($value));
        }

        return $tags;
    }

    /**
     * Group tags.
     *
     * @param Tag[] $tags
     *
     * @return Tag[]
     */
    public static function groupBy(array $tags)
    {
        $groups = [];

        if (0 === count($tags)) {
            return $groups;
        }

        foreach ($tags as $tag) {
            $className = get_class($tag);
            $groups[$className][] = $tag->getValue();
        }

        return self::getNameClass($groups);
    }

    /**
     * @param $classNamespace
     *
     * @return array
     */
    private static function getNameClass($classNamespace)
    {
        $names = [];
        foreach ($classNamespace as $class => $value) {
            if ($tmp = explode('\\', $class)) {
                $names[end($tmp)] = $value;
            }
        }

        return $names;
    }

    /**
     * Get Tag enum.
     *
     * @param $value
     */
    private static function getTagFrom($value): ?Tag
    {
        return Tag::fromTag($value);
    }

    /**
     * Clean the value
     * - remove return line.
     *
     * @return array|string|string[]
     */
    private static function cleanValue(string $value)
    {
        return str_replace(["\n", "\r"], '', $value);
    }
}
