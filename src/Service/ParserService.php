<?php

namespace App\Service;

use App\Entity\Magazine;
use App\Enums\Tag;
use App\Service\AppCodeTransformerService;

class ParserService
{
    public static $index = 0;

    /**
     * List of files under directory.
     *
     * @return false|string|null
     */
    public static function getFiles(string $directory)
    {
        return shell_exec("find $directory -type f");
    }

    /**
     * Parse main function.
     */
    public static function parse(string $directory)
    {
        $files = self::getFiles($directory);
        $files = preg_split('/\s+/', trim($files));
        $dataFromFile = [];

        // Scan each log file under a directory, get datas, parse them and generate a csv file
        foreach ($files as $file) {
            if (str_contains($file, '.log')) {
                $dataFromFile = ParserService::getDataFromFile($file);
                ParserService::csvGenerated($dataFromFile, 'result');
            }
        }

        return self::$index;
    }

    /**
     * Parse a line and convert it in an entity.
     */
    public static function parseLine(array $data, int $id): ?Magazine
    {
        $magazine = new Magazine();

        try {
            $magazine->setId($id);
            $magazine->setAppCode(AppCodeTransformerService::getInstance()->getApp($data[0]));
            $magazine->setDeviceId($data[1]);
            $magazine->setContactable('1' === $data[2]);

            $tags = TagTransformer::getTagsFrom($data[3]);

            /** @var Tag $tag */
            foreach ($tags as $tag) {
                $magazine->addTag($tag);
            }
        } catch (\Exception $exception) {
            return null;
        }

        return $magazine;
    }

    /**
     * Get data from file path.
     *
     * @return array
     */
    public static function getDataFromFile(string $file)
    {
        $file = fopen($file, 'r');
        $data = [];

        // Skip the first line
        $firstLine = fgets($file);

        //Convert first line as a clean array and add the new field id
        $firstLine = explode(',', self::cleanValue($firstLine));
        array_unshift($firstLine, 'id');

        array_push($data, $firstLine);
        while ($line = fgets($file)) {
            $magazine = ParserService::parseLine(explode(',', $line), self::$index);
            $tagsGrouped = TagTransformer::groupBy($magazine->getTags()->toArray());

            if (null !== $magazine) {
                $tmp = [
                    $magazine->getId(),
                    $magazine->getAppCode(),
                    $magazine->getDeviceId(),
                    $magazine->getContactable(),
                ];

                foreach ($tagsGrouped as $t) {
                    $tmp[] = implode('|', $t);
                }

                $data[] = $tmp;
            }
            ++self::$index;
        }
        fclose($file);

        return $data;
    }

    /**
     * Create the csv file from an array.
     */
    public static function csvGenerated(array $data, string $name)
    {
        $csvFile = fopen($_ENV['DIRECTORY_RESULT'].$name.'.csv', 'a');

        if (false === $csvFile) {
            return;
        }

        foreach ($data as $element) {
            fputcsv($csvFile, $element);
        }

        fclose($csvFile);
    }

    /**
     * Clean the value
     * - remove return line.
     *
     * @return array|string|string[]
     */
    private static function cleanValue(string $value)
    {
        return str_replace(["\n", "\r"], '', $value);
    }
}
