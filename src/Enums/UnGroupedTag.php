<?php

namespace App\Enums;

class UnGroupedTag extends Tag
{
    public function __construct(string $value)
    {
        $this->value = $value;
    }
}
