<?php

namespace App\Enums;

class SubscriptionTag extends Tag
{
    public const ACTIVE_SUBSCRIBER = 'active_subscriber';
    public const EXPIRED_SUBSCRIBER = 'expired_subscriber';
    public const NEVER_SUBSCRIBER = 'never_subscribed';
    public const UNKNOWN_SUBSCRIBER = 'subscription_unknown';
}
