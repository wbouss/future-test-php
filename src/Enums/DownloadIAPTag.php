<?php

namespace App\Enums;

class DownloadIAPTag extends Tag
{
    public const HAS_DOWNLOADED_IAP_PRODUCT = 'has_downloaded_iap_product';
    public const NOT_DOWNLOADED_FREE_PRODUCT = 'not_downloaded_free_product';
    public const DOWNLOADED_IAP_PRODUCT_UNKNOWN = 'downloaded_iap_product_unknown';
}
