<?php

namespace App\Enums;

use MyCLabs\Enum\Enum;

class Tag extends Enum
{
    public static $tagsGroup = [
            SubscriptionTag::class,
            DownloadFreeTag::class,
            DownloadIAPTag::class,
        ];

    /**
     * @return Tag
     */
    public static function fromTag(string $value)
    {
        /** @var Tag $tagGroup */
        foreach (self::$tagsGroup as $tagGroup) {
            try {
                return $tagGroup::from($value);
            } catch (\Exception $exception) {
            }
        }

        return new UnGroupedTag($value);
    }
}
