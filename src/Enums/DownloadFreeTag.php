<?php

namespace App\Enums;

class DownloadFreeTag extends Tag
{
    public const HAS_DOWNLOAD_FREE_PRODUCT = 'has_downloaded_free_product';
    public const NOT_DOWNLOADED_FREE_PRODUCT = 'not_downloaded_free_product';
    public const DOWNLOADED_FREE_PRODUCT_UNKNOWN = 'downloaded_free_product_unknown';
}
